package main

import "testing"

func TestIncrement(t *testing.T) {
	s := series{name: "Test", season: 1, episode: 1}
	s.increment()
	if s.episode != 2 {
		t.Errorf("Test episode not incremented, got: %d, wanted: 2", s.episode)
	}
}

func TestSeasonIncrement(t *testing.T) {
	s := series{name: "Test", season: 1, episode: 1}
	s.incrementSeason()
	if s.season != 2 && s.episode != 1 {
		t.Errorf("Test season not incremented, got: %d, wanted: 2", s.season)
	}
}

// Add tests for those functions
//func (as allSeries) addOrUpdate(s series) allSeries {
//func (as allSeries) del(s series) allSeries {
//func (as allSeries) show(s string) {
//func (as allSeries) write() {
//func (as allSeries) read() {
