package main

import (
	"bufio"
	"flag"
	"fmt"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
)

type allSeries map[string]series

type series struct {
	name    string
	season  int
	episode int
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func (s *series) increment() {
	s.episode++
}

func (s *series) incrementSeason() {
	s.season++
	s.episode = 1
}

func (as allSeries) addOrUpdate(s series) allSeries {
	as[s.name] = series{name: s.name, season: s.season, episode: s.episode}
	return as
}

func (as allSeries) del(s series) allSeries {
	delete(as, s.name)
	return as
}

func (as allSeries) show(s string) {
	if s != "" {
		series, ok := as[s]
		if ok {
			fmt.Printf("%s - S%02dE%02d\n", series.name, series.season, series.episode)
		}
	} else {
		keys := make([]string, 0, len(as))
		for k := range as {
			keys = append(keys, k)
		}
		sort.Strings(keys)

		for _, i := range keys {
			fmt.Printf("%s - S%02dE%02d\n", i, as[i].season, as[i].episode)
		}
	}
}

func (as allSeries) write() {
	f, err := os.Create("series.txt")
	check(err)
	defer f.Close()

	w := bufio.NewWriter(f)
	for i := range as {
		str := fmt.Sprintf("%s - S%02dE%02d\n", i, as[i].season, as[i].episode)
		w.WriteString(str)
	}
	w.Flush()
}

func (as allSeries) read() {
	f, err := os.Open("series.txt")
	check(err)
	defer f.Close()
	s := bufio.NewScanner(f)
	for s.Scan() {
		splitted := strings.Split(s.Text(), "-")
		seriesName := strings.TrimSpace(splitted[0])
		r, _ := regexp.Compile("[0-9]+")
		numbers := r.FindAllString(splitted[1], -1)
		season, _ := strconv.Atoi(numbers[0])
		episode, _ := strconv.Atoi(numbers[1])
		s := series{name: seriesName, season: season, episode: episode}
		as.addOrUpdate(s)
	}
}

func main() {
	isUpdated := false
	seriesName := flag.String("name", "", "series name")
	inc := flag.Bool("inc", false, "increment episode")
	season := flag.Bool("season", false, "increment season")
	remove := flag.Bool("remove", false, "remove series")

	flag.Parse()

	allSeries := make(allSeries)
	allSeries.read()

	if *seriesName != "" {
		if *inc {
			s := allSeries[*seriesName]
			s.increment()
			allSeries.addOrUpdate(s)
			isUpdated = true
		}
		if *season {
			s := allSeries[*seriesName]
			s.incrementSeason()
			allSeries.addOrUpdate(s)
			isUpdated = true
		}
		_, ok := allSeries[*seriesName]
		if !ok {
			s := series{name: *seriesName, season: 1, episode: 1}
			allSeries.addOrUpdate(s)
			isUpdated = true
		}
		if *remove {
			s := allSeries[*seriesName]
			allSeries.del(s)
			isUpdated = true
		}
	}
	allSeries.show(*seriesName)
	if isUpdated {
		allSeries.write()
	}
}
